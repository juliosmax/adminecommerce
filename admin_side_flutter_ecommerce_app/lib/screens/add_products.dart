import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import '../db/category.dart';
import '../db/brand.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../db/products.dart';


class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  
  Color white = Colors.white;
  Color black = Colors.black;
  Color grey = Colors.grey;
  Color red = Colors.red;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController productNameController = TextEditingController();
  TextEditingController quantityNameController = TextEditingController();
  final priceNameController=TextEditingController();
  List<DocumentSnapshot> categories = <DocumentSnapshot>[];
  List<DocumentSnapshot> brands = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> brandsDropDown = <DropdownMenuItem<String>>[];
  List<DropdownMenuItem<String>> categoriesDropDown =
      <DropdownMenuItem<String>>[];
  String _currentBrand;
  String _currentCategory = "test";
  CategoryService _categoryService = CategoryService();
  BrandService _brandService = BrandService();
  List<String> selectedSizes=<String>[];
  File _image1;
  File _image2;
  File _image3;
  ProductService productService= ProductService();
  bool isLoading=false;

  @override
  void initState() {
    _getCategories();
     _getBrands();
    //  categoriesDropDown= getCategoriesDropDown();
    // _currentCategory=categoriesDropDown[0].value;
  }

  List<DropdownMenuItem<String>> getCategoriesDropDown() {
    List<DropdownMenuItem<String>> items = List();
    for(int i=0; i<categories.length;i++){
      setState(() {
           items.insert(0, DropdownMenuItem(child: Text(categories[i].data['category']),value:categories[i].data['category'] ));
      });
    }
    return items;
  }

   List<DropdownMenuItem<String>> getBrandsDropDown() {
    List<DropdownMenuItem<String>> items = List();
    for(int i=0; i<brands.length;i++){
      setState(() {
           items.insert(0, DropdownMenuItem(child: Text(brands[i].data['brand']),value:brands[i].data['brand'] ));
      });
    }
    return items;
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: white,
        elevation: 0.1,
        leading: Icon(Icons.close, color: black),
        title: Text("Add Product", style: TextStyle(color: black)),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child:isLoading ? CircularProgressIndicator():   Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(
                          color: Colors.grey.withOpacity(0.8), width: 2.0),
                      onPressed: () {
                          selectImage(ImagePicker.pickImage(source: ImageSource.gallery),1);
                      },
                      child: _displayChild1(),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(
                          color: Colors.grey.withOpacity(0.8), width: 2.0),
                      onPressed: () {
                         selectImage(ImagePicker.pickImage(source: ImageSource.gallery),2);
                      },
                      child: _displayChild2(),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(
                          color: Colors.grey.withOpacity(0.8), width: 2.0),
                      onPressed: () {
                         selectImage(ImagePicker.pickImage(source: ImageSource.gallery),3);
                      },
                      child:  _displayChild3(),
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "Ingrese el nombre de un producto(10 caracteres maximo)",
                style: TextStyle(color: red, fontSize: 12),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: productNameController,
                decoration: InputDecoration(hintText: "Product name"),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Debes ingresar un nombre del producto";
                  } else if (value.length > 10) {
                    return "El nombre de tu producto no debe ser mayor a 10 caracteres";
                  }
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: priceNameController,
               // initialValue: '0.00',
                decoration: InputDecoration(labelText: 'Price' , hintText: "Price"),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Debes ingresar el precio del producto";
                  }
                },
              ),
            ),

            //====CATEGORIAS y BRANDS========
               Row(
                 children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Text('Category', style:TextStyle(color:Colors.red)),
                   ),
                   DropdownButton(items: categoriesDropDown, onChanged: changeSelectedCategorie,value:_currentCategory),
                     Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Text('Brand', style:TextStyle(color:Colors.red)),
                   ),
                   DropdownButton(items: brandsDropDown, onChanged: changeSelectedBrand,value:_currentBrand),
                 
                 ],
               ),
              //=====CANTIDAD======
               Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: quantityNameController,
                keyboardType: TextInputType.number,
               // initialValue: '1',
                decoration: InputDecoration(labelText: 'Quantity', hintText: "Quantity"),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Ingresa la cantidad del producto";
                  } 
                },
              ),
            ),
            //====SIZES==========
            Text('Tamaños disponibles'),
            Row(children: <Widget>[
                Checkbox(value: selectedSizes.contains('XS'), onChanged: (value)=>changeSelectedSizes('XS')),
                Text('XS'),
                Checkbox(value: selectedSizes.contains('S'), onChanged: (value)=>changeSelectedSizes('S')),
                Text('S'),
                Checkbox(value: selectedSizes.contains('M'), onChanged: (value)=>changeSelectedSizes('M')),
                Text('M'),
                Checkbox(value: selectedSizes.contains('L'), onChanged: (value)=>changeSelectedSizes('L')),
                Text('L'),
                Checkbox(value: selectedSizes.contains('xL'), onChanged: (value)=>changeSelectedSizes('xL')),
                Text('xL'),
                Checkbox(value: selectedSizes.contains('xxL'), onChanged: (value)=>changeSelectedSizes('xxL')),
                Text('xxL'),
            ]),
            Row(children: <Widget>[
                Checkbox(value: selectedSizes.contains('25'), onChanged: (value)=>changeSelectedSizes('25')),
                Text('25'),
                Checkbox(value: selectedSizes.contains('26'), onChanged: (value)=>changeSelectedSizes('26')),
                Text('26'),
                Checkbox(value: selectedSizes.contains('28'), onChanged: (value)=>changeSelectedSizes('28')),
                Text('28'),
                Checkbox(value: selectedSizes.contains('30'), onChanged: (value)=>changeSelectedSizes('30')),
                Text('30'),
                Checkbox(value: selectedSizes.contains('31'), onChanged: (value)=>changeSelectedSizes('31')),
                Text('31'),
                Checkbox(value: selectedSizes.contains('32'), onChanged: (value)=>changeSelectedSizes('32')),
                Text('32'),
            ]),
            Row(children: <Widget>[
                Checkbox(value: selectedSizes.contains('34'), onChanged: (value)=>changeSelectedSizes('34')),
                Text('34'),
                Checkbox(value: selectedSizes.contains('36'), onChanged: (value)=>changeSelectedSizes('36')),
                Text('36'),
                Checkbox(value: selectedSizes.contains('38'), onChanged: (value)=>changeSelectedSizes('38')),
                Text('38'),
                Checkbox(value: selectedSizes.contains('40'), onChanged: (value)=>changeSelectedSizes('40')),
                Text('40'),
                Checkbox(value: selectedSizes.contains('42'), onChanged: (value)=>changeSelectedSizes('42')),
                Text('42'),
                Checkbox(value: selectedSizes.contains('44'), onChanged: (value)=>changeSelectedSizes('44')),
                Text('44'),
            ]),
          
            FlatButton(onPressed: (){
                 validateAndUpload();
            }, 
            color: red,
            textColor: white,
            child: Text('Añade el Producto'),),
          ],
        ),
      ),
      ),
    
    ); 
  }

  _getCategories() async {
    List<DocumentSnapshot> data = await _categoryService.getCategories();
    print(data.length);
    setState(() {
      categories = data;
      categoriesDropDown= getCategoriesDropDown();
      _currentCategory=categories[0].data['category'];
      print("dentro de set state");
    });
  }

  _getBrands() async {
    List<DocumentSnapshot> data = await _brandService.getBrands();
    setState(() {
      brands = data;
        brandsDropDown= getBrandsDropDown();
      _currentBrand=brands[0].data['brand'];
      print("dentro de set state");
    });
  }

  changeSelectedCategorie(String selectedCategorie) {
    setState(() => _currentCategory = selectedCategorie);
  }

  changeSelectedBrand(String selectedBrand) {
    setState(() => _currentBrand = selectedBrand);
  }

  void changeSelectedSizes(String size) {
    if(selectedSizes.contains(size)){
      setState(() {
        selectedSizes.remove(size);
      });
    } else {
      setState(() {
          selectedSizes.insert(0,size);
      });
    }
  }

  void selectImage(Future<File> pickImage, int imageNumber) async {
    File tmpImage= await pickImage;
    switch(imageNumber){
      case 1: setState(() =>   _image1=tmpImage);
      break;
      case 2: setState(() =>   _image2=tmpImage);
      break;
      case 3: setState(() =>   _image3=tmpImage);
      break;
      
   }
  }
  Widget _displayChild1(){
     if(_image1==null) {
       return Padding(padding: const EdgeInsets.fromLTRB(14, 50, 14, 50),
              child: new Icon(Icons.add, color: grey) ,  );
     } else
     {
       return   Image.file(_image1, fit: BoxFit.fill, width: double.infinity,);
     }

  }
  Widget _displayChild2(){
     if(_image2==null) {
       return Padding(padding: const EdgeInsets.fromLTRB(14, 50, 14, 50),
              child: new Icon(Icons.add, color: grey) ,  );
     } else
     {
       return   Image.file(_image2, fit: BoxFit.fill, width: double.infinity,);
     }

  }

  Widget _displayChild3(){
     if(_image3==null) {
       return Padding(padding: const EdgeInsets.fromLTRB(14, 50, 14, 50),
              child: new Icon(Icons.add, color: grey) ,  );
     } else
     {
       return   Image.file(_image3, fit: BoxFit.fill, width: double.infinity,);
     }

  }

  void validateAndUpload() async{
    if(_formKey.currentState.validate()){
        setState(()=> isLoading=true );

       if(_image1!=null && _image2!=null && _image3!=null) {
          if(selectedSizes.isNotEmpty){
            String imageUrl1;
            String imageUrl2;
            String imageUrl3;
            
            final FirebaseStorage  storage= FirebaseStorage.instance;
            final String picture1 ="1${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
            StorageUploadTask task1= storage.ref().child(picture1).putFile(_image1);
            final String picture2 ="2${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
            StorageUploadTask task2= storage.ref().child(picture2).putFile(_image2);
            final String picture3 ="3${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
            StorageUploadTask task3= storage.ref().child(picture3).putFile(_image3);

            StorageTaskSnapshot snapshot1= await task1.onComplete.then((snaps) => snaps);
            StorageTaskSnapshot snapshot2= await task2.onComplete.then((snaps) => snaps);
            
            task3.onComplete.then((snapshot3) async{
              imageUrl1=await snapshot1.ref.getDownloadURL();
              imageUrl2=await snapshot2.ref.getDownloadURL();
              imageUrl3= await  snapshot3.ref.getDownloadURL();
              List<String> imageList= [imageUrl1,imageUrl2,imageUrl3];

              productService.uploadProducts(productName: productNameController.text,
               price:double.parse(priceNameController.text),
               sizes:selectedSizes,
               images: imageList,
               quantity: int.parse(quantityNameController.text));
               _formKey.currentState.reset();
               setState(()=> isLoading=false );
               Fluttertoast.showToast(msg: 'Product added');
               Navigator.pop(context);
               
            });


          } else {  setState(()=> isLoading=false );
                   Fluttertoast.showToast(msg: 'Seleccione al menos una talla'); }
       } else { setState(()=> isLoading=false );
         Fluttertoast.showToast(msg: 'todas las imagenes deben de ser proveidas');
       }
    }
  }
}
